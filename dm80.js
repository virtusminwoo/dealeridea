var express = require('express');
var mysql = require('mysql');
var bodyParser = require('body-parser');
var parseurl = require('parseurl');
var session = require('express-session');
var MySQLStore = require('express-mysql-session')(session);
var fs = require('fs');
var winston = require('winston');

var conn = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: '@Loverudal2637',
	database: 'dm1'
});

    conn.connect();

var app = express();

    app.set('views','./views');
    app.set('view engine', 'jade');
    app.use(bodyParser.urlencoded({ extended: false }));

    app.use(session({
    secret: '1234DSFs@adf1234!@#$asd',
    resave: false,
    saveUninitialized: true,
    store:new MySQLStore({
    host:'localhost',
    port:3306,
    user:'root',
    password:'@Loverudal2637',
    database:'dm1'
    })
}));

winston.level = 'debug';
winston.add(winston.transports.File, { filename: 'dealerIdea.log' });
winston.remove(winston.transports.Console);


//첫화면

app.get('' , function(req,res){
        winston.info('main page request 접속');
        res.render('layBmain');
});



app.get('/main1',function(req,res){
    fs.readFile('./images/main.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
    });
});




//내 정보 

app.get('/DM/layBmyinfo', function(req,res){
        winston.info('myinfo page request 접속');
        res.render('layBmyinfo');
});


app.get('/DM/layBmyinfo/myinfo1',function(req,res){
     winston.info('myinfo1 page request');
    fs.readFile('./images/myinfo.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
            winston.info('file read success. myInfo.png download success');
    });
});

app.get('/DM/layBmyinfo/myinfo2',function(req,res){
    fs.readFile('./images/myinfo_a.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
            winston.info('file read success. myInfo_a.png download success');
    });
});


//내 차고 

app.get('/DM/layBprivateCar', function(req,res){
        winston.info('privateCar page request 접속');
        res.render('layBprivateCar');
});


app.get('/DM/layBprivateCar/privateCar1',function(req,res){
    fs.readFile('./images/privateCar.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
            winston.info('file read success. privateCar.png download success');
    });
});

app.get('/DM/layBprivateCar/privateCar2',function(req,res){
    fs.readFile('./images/privateCar_a.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
            winston.info('file read success. privateCar_a.png download success');
    });
});

app.get('/DM/layBprivateCar/privateCar3',function(req,res){
    fs.readFile('./images/privateCar_b.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
            winston.info('file read success. privateCar_b.png download success');
    });
});

app.get('/DM/layBprivateCar/privateCar4',function(req,res){
    fs.readFile('./images/privateCar_c.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
            winston.info('file read success. privateCar_c.png download success');
    });
});

//내 상사

app.get('/DM/layBcompanyGroup', function(req,res){
        winston.info('companyGroup page request 접속');
        res.render('layBcompanyGroup');
});


app.get('/DM/layBcompanyGroup/companyGroup1',function(req,res){
    fs.readFile('./images/companyGroup.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
            winston.info('file read success. companyGroup.png download success');
    });
});

app.get('/DM/layBcompanyGroup/companyGroup2',function(req,res){
    fs.readFile('./images/companyGroup_a.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
            winston.info('file read success. companyGroup_a.png download success');
    });
});

app.get('/DM/layBcompanyGroup/companyGroup3',function(req,res){
    fs.readFile('./images/companyGroup_b.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
            winston.info('file read success. companyGroup_b.png download success');
    });
});

app.get('/DM/layBcompanyGroup/companyGroup4',function(req,res){
    fs.readFile('./images/companyGroup_c.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
            winston.info('file read success. companyGroup_c.png download success');
    });
});

app.get('/DM/layBcompanyGroup/companyGroup5',function(req,res){
    fs.readFile('./images/companyGroup_d.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
            winston.info('file read success. companyGroup_d.png download success');
    });
});


//내 그룹

app.get(['/DM/layBmyGroup'],function(req,res){
        winston.info('myGroup page request 접속');
        res.render('layBmyGroup');
});
    
app.get('/DM/layBmyGroup/myGroup1',function(req,res){
    fs.readFile('./images/myGroup.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
            winston.info('file read success. myGroup.png download success');
    });
});

app.get('/DM/layBmyGroup/myGroup2',function(req,res){
    fs.readFile('./images/myGroup_a.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
            winston.info('file read success. myGroup_a.png download success');
    });
});

app.get('/DM/layBmyGroup/myGroup3',function(req,res){
    fs.readFile('./images/myGroup_b.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
            winston.info('file read success. myGroup_b.png download success');
    });
});

app.get('/DM/layBmyGroup/myGroup4',function(req,res){
    fs.readFile('./images/myGroup_c.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
            winston.info('file read success. myGroup_c.png download success');
    });
});

app.get('/DM/layBmyGroup/myGroup5',function(req,res){
    fs.readFile('./images/myGroup_d.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
            winston.info('file read success. myGroup_d.png download success');
    });
});


//찜한 차    myCart.jade  

app.get(['/DM/layBmyCart'],function(req,res){
        winston.info('myCart page request 접속');
        res.render('layBmyCart');
});

app.get('/DM/layBmyCart/myCart1',function(req,res){
    fs.readFile('./images/myCart.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
            winston.info('file read success. myCart.png download success');
    });
});

app.get('/DM/layBmyCart/myCart2',function(req,res){
    fs.readFile('./images/myCart_a.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
            winston.info('file read success. myCart_a.png download success');
    });
});



//사용자 찾기

app.get(['/DM/layBuserInfo'],function(req,res){
        winston.info('myCart page request 접속');
        res.render('layBuserInfo');
});
    


app.get('/DM/layBuserInfo/userInfo1',function(req,res){
    fs.readFile('./images/userInfo.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
            winston.info('file read success. /userInfo.png download success');
    });
});


//전국매물정보

app.get('/DM/layBallCarList', function(req,res){
        winston.info('allCarList page request 접속');
        res.render('layBallCarList');
});


app.get('/DM/layBallCarList/allCarList1',function(req,res){
    fs.readFile('./images/allCarList.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
            winston.info('file read success. /allCarList.png download success');
    });
});


app.get('/DM/layBallCarList/allCarList2',function(req,res){
    fs.readFile('./images/allcarList_a.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
            winston.info('file read success. /allCarList_a.png download success');
    });
});

app.get('/DM/layBallCarList/allCarList3',function(req,res){
    fs.readFile('./images/allCarList_b.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
            winston.info('file read success. /allCarList_b.png download success');
    });
});


app.get('/DM/layBallCarList/allCarList4',function(req,res){
    fs.readFile('./images/allCarList_c.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
            winston.info('file read success. /allCarList_c.png download success');
    });
});








//리스사 정보 

app.get('/DM/layBleaseCompany', function(req,res){
        winston.info('leaseCompany page request 접속');
        res.render('layBleaseCompany');
});





//리스 이자율 계산기

app.get('/DM/layBleaseInterestRate',function(req,res){
        winston.info('leaseInterestRate page request 접속');
        res.render('layBleaseInterestRate');
});
    


//리스 이자율 계산기 insert

app.post('/DM/layBleaseInterestRate/layBleaseInterestRateInsert',function(req,res){
    var interest = {
    carName : req.body.carName,
    carNumber : req.body.carNumber,
    leaseCompany : req.body.leaseCompany,
    leaseForm : req.body.leaseForm,
    leaseFixedDate : req.body.leaseFixedDate,
    allInning : req.body.allInning,
    nowInning : req.body.nowInning,
    residualInning : req.body.allInning-req.body.nowInning,
    leaseFee : req.body.leaseFee,
    prePaid : req.body.prePaid,
    nonPrincipal : req.body.nonPrincipal,
    deposit : req.body.deposit,
    residualValue : req.body.residualValue,
    purePrincipal : req.body.nonPrincipal-req.body.deposit,
    veryPurePrincipal : req.body.nonPrincipal-req.body.deposit-req.body.prePaid*(req.body.allInning-req.body.nowInning),
    repayment : req.body.deposit-req.body.residualValue,
    interestRate : [req.body.leaseFee-(req.body.nonPrincipal-req.body.nonPrincipal2)]*12/req.body.nonPrincipal,
    interestRate2 : [req.body.leaseFee-(req.body.nonPrincipal2-req.body.nonPrincipal3)]*12/req.body.nonPrincipal2,
    interestRate3 : [req.body.leaseFee-(req.body.nonPrincipal3-req.body.nonPrincipal4)]*12/req.body.nonPrincipal3,
    interestRate4 : [req.body.leaseFee-(req.body.nonPrincipal4-req.body.nonPrincipal5)]*12/req.body.nonPrincipal4,
    allInterest : req.body.leaseFee*(req.body.allInning-req.body.nowInning)-(req.body.nonPrincipal-req.body.residualValue),
    residualPrePaid : req.body.prePaid*(req.body.allInning-req.body.nowInning),
    conditionChange : req.body.conditionChange,
    nonPrincipal2 : req.body.nonPrincipal2,
    nonPrincipal3 : req.body.nonPrincipal3,
    nonPrincipal4 : req.body.nonPrincipal4,
    nonPrincipal5 : req.body.nonPrincipal5
    };
    var interestSql = 'INSERT INTO leaseinterestrate SET ?';
        conn.query(interestSql,interest, function(err, row, fields){
    if(err){
        console.log("DB Error Occurred");
        return;
    } else {
        res.render('layBleaseInterestRateResult',{row:interest});
        };
        });
        });
 

app.get('/DM/layBleaseInterestRate/layBleaseInterestRateInsert', function(req,res){
        winston.info('leaseInterestRateInsert page request 접속');
        res.render('layBleaseInterestRateInsert');
});
       


//리스이자율 설명서

app.get('/DM/layBleaseInterestRateManual', function(req,res){
        winston.info('leaseInterestRateManual page request 접속');
        res.render('layBleaseInterestRateManual');
});



//리스이자율 설명서 개별 설명

app.get('/DM/layBleaseInterestRateManual/financeLeaseNormal',function(req,res){
    fs.readFile('./images/financeLeaseNormal.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
            winston.info('file read success. /financeLeaseNormal.png download success');
    });
});

app.get('/DM/layBleaseInterestRateManual/operatingLeasePrepaid',function(req,res){
    fs.readFile('./images/operatingLeasePrepaid.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
            winston.info('file read success. /operatingLeasePrepaid.png download success');
    });
});

app.get('/DM/layBleaseInterestRateManual/operatingLeaseCarTax',function(req,res){
    fs.readFile('./images/operatingLeaseCarTax.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
            winston.info('file read success. /operatingLeaseCarTax.png download success');
    });
});

app.get('/DM/layBleaseInterestRateManual/postponementLeaseNormal',function(req,res){
    fs.readFile('./images/postponementLeaseNormal.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
            winston.info('file read success. /postponementLeaseNormal.png download success');
    });
});





// 신차정보

app.get('/DM/layBnewCarInfo/',function(req,res){
    res.render('layBnewCarInfo');
    });



app.get('/DM/layBnewCarInfo/newCarInfo1',function(req,res){
    fs.readFile('./images/newCarInfo.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
    });
});

app.get('/DM/layBnewCarInfo/newCarInfo2',function(req,res){
    fs.readFile('./images/newCarInfo_a.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
    });
});

app.get('/DM/layBnewCarInfo/newCarInfo3',function(req,res){
    fs.readFile('./images/newCarInfo_b.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
    });
});







//DM칼럼 게시판 

app.get('/DM/layBboardDM/',function(req,res){
        winston.info('boardDM page request 접속');
        res.redirect('/DM/layBboardDM/list/1');
});


//DM칼럼 게시판 boardDMList   boardDMList.jade

app.get('/DM/layBboardDM/list/:page',function(req,res){
    var forSelectListSql = "SELECT * FROM boarddm";
        conn.query(forSelectListSql,function(err,rows){
    if(err){
        console.log("DB Error Occurred List");
        winston.error("DI게시판 list 처리 과정에서 db error 발생", err);
    } else { 
        res.render('layBboardDMList',{rows:rows});
        }
        });
        });


//DM칼럼 게시판 boardDMWrite   boardDMWrite.jade

app.post('/DM/boardDM/write', function(req,res,next){
    var displayName = req.body.displayName;
    var title = req.body.title;
    var content = req.body.content;
    var password = req.body.password;
    
    var forInsertBoardSql = "INSERT INTO boarddm (displayName, title, content, password) values (?,?,?,?)";
        conn.query(forInsertBoardSql, boardDM, function(err,rows){
    if(err){
        console.log("DB Error Occurred Write");
        winston.error("DI게시판 write 처리 과정에서 db error 발생", err);
    } else { 
        res.redirect('/DM/boardDM');
        }
        });
        });



app.get('/DM/boardDM/write', function(req,res,next){
        winston.info('boarddm write page request 접속');
        res.render('boardDMWrite');
});


//DM칼럼 게시판 boardDMRead   boardDMRead.jade 조회 및 수정

app.get('/DM/layBboardDM/read/:boardId',function(req,res,next){
    var boardId = req.params.boardId;
    var sql = "SELECT * FROM boarddm where boardId=?";
        conn.query(sql,[boardId], function(err,row){
    if(err){
        console.log("DB Error Occurred Read");
        winston.error("DI게시판 read 처리 과정에서 db error 발생", err);
    } else {
        res.render('layBboardDMRead', {row:row[0]});
        }    
        });
        });


//DM칼럼 게시판 boardDMModify   boardDMModify.jade 조회 및 수정
app.post('/DM/boardDM/modify/:boardId',function(req,res,next){
    var boardId = req.params.boardId;
    var displayName = req.body.displayName;
    var title = req.body.title;
    var content = req.body.content;
    var password = req.body.password;

    var sql = "UPDATE boarddm SET displayName=? , title=?, content=?, regdate=now() where boardId=? and password=?";
        conn.query(sql,[displayName,title,content,boardId,password],function(err,row){
    if(err){
        console.error("DB Error Occurred Read modify")
        winston.error("DI게시판 modify post 처리 과정에서 db error 발생", err);
    } else {
        res.redirect('/DM/boardDM/read/',{row:row});
        }
        });
        });



app.get('/DM/boardDM/modify/:boardId',function(req,res,next){
    var boardId = req.params.boardId;
    var sql = "SELECT boardId, displayName, title, content, date_format(modidate,'%Y-%m-%d %H:%i:%s') modidate, hit FROM boarddm WHERE boardId=?";
        conn.query(sql, [boardId], function(err,row){
    if(err){
        console.log("DB Error Occurred");
        winston.error("DI게시판 List get처리 과정에서 db error 발생", err);
    } else {
        res.render('boardDMModify', {row:row[0]});
        }
        });
        });

















































//서버 연결

app.listen(80, function(){
    console.log('80 OPEN');
    winston.info('server on');
});





// 코드 인덴트 (줄맞추기를 다 한다0)
// 로그를 최대한 자세히 남긴다
// grep 이라는 명령어에 대해서 알아본다

// require 에 대해서 배워본다
// 한 파일에 길쭉하게 내용이 없도록
// 페이지 or 기능별로 js 파일이 나오게
// ex > 형 로그인 기능은 어디서 봐요?
// dm80.js 에 몇번째줄 (테호가 화낼 대답)
// login.js 에 몇번쨰줄 (테호가 좋아할 정답)
    